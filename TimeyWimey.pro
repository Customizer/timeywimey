#-------------------------------------------------
#
# Project created by QtCreator 2015-11-18T19:07:04
#
#-------------------------------------------------

INSTALL_PATH = "/home/pi"

TEMPLATE = app
QT += qml quick multimedia network widgets
CONFIG += c++11
TARGET = TimeyWimey
LIBS += -lwiringPi

RESOURCES += resources.qrc

DISTFILES += timeywimey.astylerc

SOURCES += \
    libs/tsl2561/src/tsl2561.c \
    src/alarms/alarm.cpp \
    src/alarms/alarmmanager.cpp \
    src/lightsensortracker.cpp \
    src/main.cpp \
    src/settingsmanager.cpp \
    src/systemhelpers.cpp \
    src/mediaplayer/volumecontrol.cpp \
    src/alarms/alarmeditor.cpp \
    src/mediaplayer/mediaplayer.cpp \
    src/mediaplayer/radiostream.cpp \
    src/backlightdimmer.cpp \
    src/alarms/alarmlistmodel.cpp \
    src/pitftbuttonhandler.cpp \
    src/wiringpitrigger.cpp

HEADERS  += \
    libs/tsl2561/src/i2c-dev.h \
    libs/tsl2561/src/tsl2561.h \
    src/alarms/alarm.h \
    src/alarms/alarmmanager.h \
    src/lightsensortracker.h \
    src/settingsmanager.h \
    src/systemhelpers.h \
    src/mediaplayer/volumecontrol.h \
    src/alarms/alarmeditor.h \
    src/mediaplayer/mediaplayer.h \
    src/mediaplayer/radiostream.h \
    src/backlightdimmer.h \
    src/alarms/alarmlistmodel.h \
    src/pitftbuttonhandler.h \
    src/wiringpitrigger.h

# copy executable to pi dir
target.path = $$INSTALL_PATH
INSTALLS += target
