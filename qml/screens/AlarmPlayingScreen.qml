import QtQuick 2.0

import "qrc:/qml/elements"

Screen {
    property alias mouseArea: mouseArea
    Clock {
        id: time
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        width: parent.width

        fontPixelSize: 80
        anchors.topMargin: 0
    }

    ArtistAndTitleBanner {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }
}
