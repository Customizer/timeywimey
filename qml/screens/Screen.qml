import QtQuick 2.0

Rectangle {
    id: currentScreen
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.topMargin: 5
    width: parent.width
    height: parent.height - 2 * anchors.topMargin
    color: "#00FFFFFF"
    state: "inactive"

    states: [
        State {
            name: "active"
            PropertyChanges {
                target: currentScreen
                opacity: 1
            }
            PropertyChanges {
                target: currentScreen
                anchors.leftMargin: 0
            }
        },
        State {
            name: "inactive"
            PropertyChanges {
                target: currentScreen
                opacity: 0
            }
            PropertyChanges {
                target: currentScreen
                anchors.leftMargin: 500
            }
        }
    ]

    transitions: [
        Transition {
            from: "active"
            to: "*"
            SequentialAnimation {
                NumberAnimation {
                    target: currentScreen
                    properties: "opacity"
                    easing {
                        type: Easing.OutQuart
                    }
                    duration: animDuration
                }
                NumberAnimation {
                    target: currentScreen
                    property: "anchors.leftMargin"
                    duration: 0
                }
            }
        },
        Transition {
            from: "*"
            to: "active"
            SequentialAnimation {
                PauseAnimation {
                    duration: animDuration * 2 / 3
                }
                NumberAnimation {
                    target: currentScreen
                    properties: "opacity"
                    easing {
                        type: Easing.OutQuart
                    }
                    duration: animDuration
                }
            }
        }
    ]
}
