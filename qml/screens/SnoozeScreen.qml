import QtQuick 2.0

import "qrc:/qml/elements"

Screen {
    property alias mouseArea: mouseArea

    Clock {
        id: time
        anchors.left: parent.left
        anchors.verticalCenter: circleSelector.verticalCenter
        width: parent.width - circleSelector.width

        fontPixelSize: 65
        anchors.topMargin: 0
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
    }

    CircleSelector {
        id: circleSelector

        anchors.right: parent.right
        width: 120
        to: 20

        Component.onCompleted: AlarmManager.onSnoozeTimeUpdate.connect(changeSnoozeTime)

        onValueChanged: {
            AlarmManager.setSnoozeTime(circleSelector.calcExternalValueFromInternalValue())
        }

        function changeSnoozeTime(minutes) {
            setValue(minutes)
        }
    }
}
