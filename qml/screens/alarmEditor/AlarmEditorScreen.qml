import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQml.Models 2.2

import "qrc:/qml/screens"
import "qrc:/qml/elements"
import "qrc:/qml/elements/models"
import "qrc:/qml/elements/timeListView"

Screen {
    color: "#00000000"

    property int editAlarmIndex: -1

    property alias hourMouseArea: hourMouseArea
    property alias minutesMouseArea: minutesMouseArea
    property alias okButtonMouseArea: okButtonMouseArea
    property alias cancelButtonMouseArea: cancelButtonMouseArea

    Rectangle {
        id: weekdays
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        width: parent.width
        height: monday.height
        color: "#00000000"

        RowLayout {
            anchors.fill: parent
            spacing: 0
            Weekday {
                id: monday
                name: "Mo"
                qtIndex: 1
            }
            Weekday {
                id: tuesday
                name: "Tue"
                qtIndex: 2
            }
            Weekday {
                id: wednesday
                name: "Wed"
                qtIndex: 3
            }
            Weekday {
                id: thursday
                name: "Thu"
                qtIndex: 4
            }
            Weekday {
                id: friday
                name: "Fri"
                qtIndex: 5
            }
            Weekday {
                id: saturday
                name: "Sat"
                qtIndex: 6
            }
            Weekday {
                id: sunday
                name: "Sun"
                qtIndex: 7
            }
        }
    }

    HourModelFine {
        id: hourListModel
    }
    MinuteModelFine {
        id: minuteListModel
    }

    TimeListView {
        id: hourListView
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 25

        fontSize: 18

        model: hourListModel

        onCurrentItemChanged: {
            var value = model.get(currentIndex).value
            alarmTimeHours.text = value
            AlarmEditor.hoursChanged(value)
            hourChooser.setValue(value)
        }
    }

    Text {
        id: alarmTimeHours

        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 25

        color: "white"
        text: "00"
        font.pixelSize: 18
        font.bold: true
        font.family: robotoBold

        MouseArea {
            id: hourMouseArea
            anchors.fill: parent
        }
    }
    Text {
        id: alarmTimeColon

        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 55

        color: "white"
        text: ":"
        font.pixelSize: 18
        font.bold: true
        font.family: robotoBold
    }

    TimeListView {
        id: minutesListView
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 25

        fontSize: 18

        model: minuteListModel

        onCurrentItemChanged: {
            var value = model.get(currentIndex).value
            alarmTimeMinutes.text = value
            AlarmEditor.minutesChanged(value)
            minuteChooser.setValue(value)
        }
    }

    Text {
        id: alarmTimeMinutes

        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.leftMargin: 65

        color: "white"
        text: "00"
        font.pixelSize: 18
        font.bold: true
        font.family: robotoBold

        MouseArea {
            id: minutesMouseArea
            anchors.fill: parent
        }

    }

    CircleSelector {
        id: hourChooser

        anchors.left: alarmTimeColon.right
        leftMargin: 70
        anchors.top: weekdays.bottom
        anchors.topMargin: 10
        width: 130

        to: 23

        onValueChanged: {
            var value = calcExternalValueFromInternalValue();
            if (value.length < 2)
            {
                value = "0" + value
            }

            alarmTimeHours.text = value
            AlarmEditor.hoursChanged(value)
            hourListView.setValue(value)
        }
    }

    CircleSelector {
        id: minuteChooser

        anchors.left: alarmTimeColon.right
        anchors.top: weekdays.bottom
        leftMargin: 70
        anchors.topMargin: 10
        width: 130

        to: 59

        onValueChanged: {
            var value = calcExternalValueFromInternalValue();
            if (value.length < 2)
            {
                value = "0" + value
            }

            alarmTimeMinutes.text = value
            AlarmEditor.minutesChanged(value)
            minutesListView.setValue(value)
        }
    }

    RowLayout {
        anchors.top: alarmTimeColon.bottom
        anchors.topMargin: 50
        anchors.horizontalCenter: alarmTimeColon.horizontalCenter

        spacing: 10

        Image {
            id: okButton
            source: "qrc:/graphics/okButton.png"
            height: 23
            width: 24

            MouseArea {
                id: okButtonMouseArea
                anchors.fill: parent
            }
        }

        Image {
            id: cancelButton
            source: "qrc:/graphics/cancelButton.png"
            height: 23
            width: 24

            MouseArea {
                id: cancelButtonMouseArea
                anchors.fill: parent
            }
        }
    }

    function switchToHours() {
        // TODO move all this stuff to a state
        hourChooser.state = "active"
        minuteChooser.state = "inactive"

        alarmTimeHours.anchors.leftMargin = 500
        hourListView.anchors.leftMargin = 25

        alarmTimeMinutes.anchors.leftMargin = 65
        minutesListView.anchors.leftMargin = 500
    }

    function switchToMinutes() {
        minuteChooser.state = "active"
        hourChooser.state = "inactive"

        alarmTimeHours.anchors.leftMargin = 25
        hourListView.anchors.leftMargin = 500

        alarmTimeMinutes.anchors.leftMargin = 500
        minutesListView.anchors.leftMargin = 65
    }

    function refresh() {
        hourChooser.setValue(AlarmEditor.getHours())
        minuteChooser.setValue(AlarmEditor.getMinutes())

        monday.state = AlarmEditor.isWeekdayEnabled(monday.qtIndex) ? "active" : "inactive";
        tuesday.state = AlarmEditor.isWeekdayEnabled(tuesday.qtIndex) ? "active" : "inactive";
        wednesday.state = AlarmEditor.isWeekdayEnabled(wednesday.qtIndex) ? "active" : "inactive";
        thursday.state = AlarmEditor.isWeekdayEnabled(thursday.qtIndex) ? "active" : "inactive";
        friday.state = AlarmEditor.isWeekdayEnabled(friday.qtIndex) ? "active" : "inactive";
        saturday.state = AlarmEditor.isWeekdayEnabled(saturday.qtIndex) ? "active" : "inactive";
        sunday.state = AlarmEditor.isWeekdayEnabled(sunday.qtIndex) ? "active" : "inactive";
    }

}
