import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3

import "qrc:/qml/elements"
import "qrc:/qml/screens"

Screen {
    state: "inactive"
    anchors.fill: parent
    anchors.topMargin: 0

    property string borderTintColor: "#99000000"
    property string tintColor: "#BB000000"
    property string borderColor:"#d2c210"

    property alias mouseAreaHome: mouseAreaHome
    property alias mouseAreaAlarmEditor: mouseAreaAlarmEditor
    property alias mouseAreaSlumber: mouseAreaSlumber

    signal exitNavi

    MouseArea {
        /*
         * This mouse area not only enables the user to exit the navigation menu, it also
         * automatically disables all the mouse areas in the back.
         */
        id: naviMainMouseArea
        anchors.fill: parent

        onClicked: exitNavi()
    }

    Rectangle {
        id: topBorder
        width: parent.width
        height: view.smallBorderSize

        anchors.top: parent.top
        anchors.left: parent.left

        color: borderTintColor
    }

    Rectangle {
        id: yellowBorderTop
        width: parent.width
        height: 1

        anchors.top: topBorder.bottom
        anchors.left: parent.left

        color: borderColor
    }

    Rectangle {
        id: mainArea
        width: parent.width
        height: 240 - yellowBorderTop.height - topBorder.height - bottom.height - yellowBorderBottom.height

        anchors.top: yellowBorderTop.bottom
        anchors.left: parent.left

        color: tintColor

        GridLayout {
            columns: 3
            rows: 2

            anchors.fill: parent

            NavigationItem {
                id: homeNavi
                source: "qrc:/graphics/home.png"
                label: qsTr("Home")

                MouseArea {
                    id: mouseAreaHome
                    anchors.fill: parent
                }
            }
            NavigationItem {
                id: alarmEditorNavi
                source: "qrc:/graphics/alarms.png"
                label: qsTr("Alarms")

                MouseArea {
                    id: mouseAreaAlarmEditor
                    anchors.fill: parent
                }
            }
            NavigationItem {
                id: slumberNavi
                source: "qrc:/graphics/slumber.png"
                label: qsTr("Slumber")
                MouseArea {
                    id: mouseAreaSlumber
                    anchors.fill: parent
                    onClicked: {
                        MediaPlayer.startSlumberMode()
                    }
                }
            }
            NavigationItem {
                id: shutdownNavi
                source: "qrc:/graphics/shutdown.png"
                label: qsTr("Shutdown")
                MouseArea {
                    id: mouseAreaShutdown
                    anchors.fill: parent
                    onClicked: {
                        SystemHelpers.shutdown()
                    }
                }
            }
            NavigationItem {
                id: rebootNavi
                source: "qrc:/graphics/shutdown.png"
                label: qsTr("Reboot")
                MouseArea {
                    id: mouseAreaReboot
                    anchors.fill: parent
                    onClicked: {
                        SystemHelpers.reboot()
                    }
                }
            }
        }
    }

    Rectangle {
        id: yellowBorderBottom
        width: parent.width
        height: 1

        anchors.top: mainArea.bottom
        anchors.left: parent.left

        color: borderColor
    }

    Rectangle {
        id: bottom
        width: parent.width
        height: view.smallBorderSize

        anchors.top: yellowBorderBottom.bottom
        anchors.left: parent.left

        color: borderTintColor
    }
}
