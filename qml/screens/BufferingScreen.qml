import QtQuick 2.5
import QtQuick.Controls 1.5
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Screen {
    signal bufferingFinished

    Text {
        color: view.blue
        anchors.centerIn: parent
        font.family: robotoBold
        font.pixelSize: 13
        font.bold: true

        text: "Filling buffer…"
    }

    Timer {
        id: timer
        interval: 100
        repeat: true
        running: false

        // TODO replace this timer
        onTriggered: {
            if (parent.state === "active") {
                if (MediaPlayer.isPlaying()) {
                    bufferingFinished()
                }
            }
            else if (MediaPlayer.isPlaying()){
                timer.stop()
            }
        }
    }

    function startWatching() {
        timer.start()
    }
}
