import QtQuick 2.5
import QtQuick.Controls 1.5

Rectangle {
    id: base
    width: 320
    height: 240
    state: "base"

    clip: true
    property alias view: view

    property int animDuration: 280

    property string roboto: SystemHelpers.getFont()
    property string robotoBold: SystemHelpers.getBoldFont()

    View {
        id: view
    }

    Component.onCompleted: {
        if (SystemHelpers.isRunningOnPi()) {
            view.anchors.topMargin = 40
            view.anchors.leftMargin = -40
            view.rotation = 90
        }
    }
}
