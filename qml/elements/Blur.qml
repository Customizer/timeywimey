import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

FastBlur {
    id: blur
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.topMargin: -parent.parent.anchors.topMargin
    width: wallpaper.width
    height: wallpaper.height
    source: wallpaper
    radius: 23
}
