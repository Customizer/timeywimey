import QtQuick 2.0

Rectangle {
    property string borderColor: "#55FFFFFF"

    Rectangle {
        color: borderColor
        height: 1
        width: parent.width
        anchors.left: parent.left
        anchors.top: parent.top
    }

    Rectangle {
        color: borderColor
        height: 1
        width: parent.width
        anchors.left: parent.left
        anchors.bottom: parent.bottom
    }
}
