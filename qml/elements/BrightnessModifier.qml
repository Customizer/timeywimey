import QtQuick 2.5
import QtQuick.Controls 1.5
import QtGraphicalEffects 1.0
import QtQuick.Layouts 1.2

Rectangle {
    color: "#00000000"
    anchors.fill: parent

    Behavior on color {
        ColorAnimation {
            duration: animDuration
        }
    }
}
