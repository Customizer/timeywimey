import QtQuick 2.0

Text {
    // own qml file for titles
    id: title
    color: "#ffffff"
    font.family: roboto
    font.bold: true
    anchors.left: parent.left
    anchors.top: parent.top
}
