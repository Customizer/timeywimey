import QtQuick 2.0

Item {
    id: marqueeItem
    height: parent.height
    width: parent.width
    x: 99999999

    signal widthChanged

    onWidthChanged: {
        fadeOutAnimation.start()
    }
    
    NumberAnimation on x {
        id: marqueeAnimation
        from: parent.width
        to: -marqueeItem.width
        loops: Animation.Infinite
        duration: 15000
        running: false
        
        onStarted: {
            marqueeItem.opacity = 1
        }
    }
    
    NumberAnimation on opacity {
        id: fadeOutAnimation
        from: 1
        to: 0
        duration: 800
        running: false
        
        onStopped:
        {
            marqueeAnimation.stop()
            marqueeAnimation.start()
        }
    }
}
