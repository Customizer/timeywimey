/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "alarmeditor.h"

AlarmEditor::AlarmEditor(SettingsManager *settings, QObject *parent) : QObject(parent)
{
    this->settings = settings;
}

void AlarmEditor::createNewAlarm()
{
    alarm = Alarm();
    alarmNr = -1;
    editing = true;
}

void AlarmEditor::startEditing(int alarmNr)
{
    alarm = settings->getAlarm(alarmNr);
    this->alarmNr = alarmNr;
    editing = true;
}

void AlarmEditor::confirmAlarm()
{
    if (alarmNr < 0) {
        settings->addAlarm(alarm);
    } else {
        settings->saveAlarm(alarmNr, alarm);
    }
    alarm = Alarm();
    editing = false;
}

void AlarmEditor::abortAlarmEditing()
{
    alarm = Alarm();
    editing = false;
}

bool AlarmEditor::isEditing()
{
    return editing;
}

QString AlarmEditor::getHours()
{
    return alarm.getTime().toString("hh");
}

QString AlarmEditor::getMinutes()
{
    return alarm.getTime().toString("mm");
}

bool AlarmEditor::isWeekdayEnabled(int weekday)
{
    return alarm.getWeekdays().contains(static_cast<Qt::DayOfWeek>(weekday));
}

void AlarmEditor::setWeekdayEnabled(int weekday, bool enabled)
{
    if (alarmNr != -1) {
        alarm.setWeekday(weekday, enabled);
        emit settings->alarmChanged(alarmNr);
    }
}

void AlarmEditor::hoursChanged(QString hours)
{
    if (alarmNr != -1) {
        alarm.setHours(hours);
        emit settings->alarmChanged(alarmNr);
    }
}

void AlarmEditor::minutesChanged(QString minutes)
{
    if (alarmNr != -1) {
        alarm.setMinutes(minutes);
        emit settings->alarmChanged(alarmNr);
    }
}

Alarm AlarmEditor::getAlarm() const
{
    return alarm;
}

int AlarmEditor::getAlarmNr() const
{
    return alarmNr;
}
