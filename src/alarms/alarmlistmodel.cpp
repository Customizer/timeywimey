/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "alarmlistmodel.h"

AlarmListModel::AlarmListModel(SettingsManager *settings,
                               QObject *parent) : QAbstractListModel(parent)
{
    this->settings = settings;
    addAlarms(settings->getAlarms());
    QObject::connect(settings, SIGNAL(alarmChanged(int)),
                     this, SLOT(alarmChanged(int)));
    QObject::connect(settings, SIGNAL(alarmRemoved(int)),
                     this, SLOT(alarmRemoved(int)));
}

QHash<int, QByteArray> AlarmListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TimeRole] = "time";
    roles[DaysRole] = "days";
    roles[EnabledRole] = "alarmEnabled";
    return roles;
}


int AlarmListModel::rowCount(const QModelIndex &parent) const
{
    return items.size();
}

QVariant AlarmListModel::data(const QModelIndex &index, int role) const
{
    Alarm alarm = items.at(index.row());
    if (role == TimeRole) {
        return alarm.getTime().toString("hh:mm");
    } else if (role == DaysRole) {
        QString days = "";
        QList<Qt::DayOfWeek> weekdays = QList<Qt::DayOfWeek>::fromSet(alarm.getWeekdays());
        qSort(weekdays.begin(), weekdays.end());
        for (const Qt::DayOfWeek day : weekdays) {
            days += " ";
            days += QDate::shortDayName(day);
        }

        return days;
    } else if (role == EnabledRole) {
        return QVariant(alarm.isEnabled());
    }

    return "";
}

void AlarmListModel::addAlarm(Alarm alarm)
{
    beginInsertRows(QModelIndex(), items.size(), items.size());
    items.append(alarm);
    endInsertRows();
}

void AlarmListModel::addAlarms(QList<Alarm> alarms)
{
    for (const Alarm alarm : alarms) {
        addAlarm(alarm);
    }
}

void AlarmListModel::alarmChanged(int alarmNr)
{
    Alarm alarm = settings->getAlarm(alarmNr);
    if (alarmNr >= items.size()) {
        addAlarm(alarm);
    } else {
        items.replace(alarmNr, alarm);
        emit dataChanged(index(alarmNr, 0), index(alarmNr, 0));
    }
}

void AlarmListModel::alarmRemoved(int alarmNr)
{
    beginRemoveRows(QModelIndex(), alarmNr, alarmNr);
    items.removeAt(alarmNr);
    endRemoveRows();
}
