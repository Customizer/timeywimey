/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef ALARMMANAGER_H
#define ALARMMANAGER_H

#include <QObject>
#include <QTimer>

#include "../settingsmanager.h"
#include "../mediaplayer/mediaplayer.h"
#include "../alarms/alarmeditor.h"

class AlarmManager : public QObject
{
    Q_OBJECT
public:
    explicit AlarmManager(SettingsManager *settings,
                          MediaPlayer *player,
                          AlarmEditor *editor,
                          QObject *parent = 0);

    Q_INVOKABLE QString getNextAlarmString();

    Q_INVOKABLE QString getTimeToNextAlarmString();

    Q_INVOKABLE void snooze();

    Q_INVOKABLE void setSnoozeTime(QString minutes);

    Q_INVOKABLE void stopSnoozing();

    Q_INVOKABLE void stopAlarm();


signals:
    void alarmTriggered();
    void snoozeTimeUpdate(int minutes);

public slots:

private slots:
    void alarmCheckTimeout();
    void snoozeCheck();

private:
    SettingsManager *settings;
    MediaPlayer *player;
    AlarmEditor *editor;

    QDateTime reference;
    int snoozeMinutes = 0;
    QTimer snoozeTimer;
    QTimer alarmCheckTimer;

    QDateTime getNextAlarm(bool considerEditedAlarm);
    QList<QDateTime> getPotentialAlarms(bool considerEditedAlarm);
    QString createAlarmString(QDateTime alarmIntern);
    QString createTimeToAlarmString(QDateTime alarmIntern);
    void startAlarm();
};

#endif // ALARMMANAGER_H
