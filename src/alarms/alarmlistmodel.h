/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef ALARMSLISTMODEL_H
#define ALARMSLISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QList>

#include "alarm.h"
#include "../settingsmanager.h"

class AlarmListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum AlarmRoles {
        TimeRole = Qt::UserRole + 1,
        DaysRole,
        EnabledRole
    };

    AlarmListModel(SettingsManager *settings, QObject *parent = 0);

    virtual QHash<int, QByteArray> roleNames() const override;

    virtual int rowCount(const QModelIndex &parent) const override;

    virtual QVariant data(const QModelIndex &index, int role) const override;

    void addAlarm(Alarm alarm);

    void addAlarms(QList<Alarm> alarms);

private:
    QList<Alarm> items;
    SettingsManager *settings;

private slots:
    void alarmChanged(int alarmNr);
    void alarmRemoved(int alarmNr);
};

#endif // ALARMSLISTMODEL_H
