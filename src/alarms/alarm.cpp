/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "alarm.h"

Alarm::Alarm()
{
    time = QTime(0, 0);

    weekdays << Qt::Monday;
    weekdays << Qt::Tuesday;
    weekdays << Qt::Wednesday;
    weekdays << Qt::Thursday;
    weekdays << Qt::Friday;
    weekdays << Qt::Saturday;
    weekdays << Qt::Sunday;
}

Alarm::Alarm(QString hours, QString minutes, QStringList weekdays, bool enabled)
{
    time.setHMS(hours.toInt(), minutes.toInt(), 0);
    for (const QString weekday : weekdays) {
        Qt::DayOfWeek dayOfWeek = static_cast<Qt::DayOfWeek>(weekday.toInt());
        setWeekday(dayOfWeek, true);
    }
    this->enabled = enabled;
}

QSet<Qt::DayOfWeek> Alarm::getWeekdays()
{
    return weekdays;
}

QTime Alarm::getTime()
{
    return time;
}

QList<QDateTime> Alarm::getAlarmDateTimes(QDateTime reference) const
{
    QList<QDateTime> alarms;
    for (const Qt::DayOfWeek day : weekdays) {
        int referenceDayOfWeek = reference.date().dayOfWeek();
        int dayOffset = day - referenceDayOfWeek;
        if (dayOffset < 0 ||
                (dayOffset == 0 && reference.time() > time)) {
            dayOffset += 7;
        }
        QDateTime alarm(reference.date().addDays(dayOffset));
        alarm.setTime(time);
        alarms += alarm;
    }

    return alarms;
}

void Alarm::setHours(QString hours)
{
    time.setHMS(hours.toInt(), time.minute(), 0);
}

void Alarm::setMinutes(QString minutes)
{
    time.setHMS(time.hour(), minutes.toInt(), 0);
}

void Alarm::setWeekday(int day, bool enabled)
{
    setWeekday(static_cast<Qt::DayOfWeek>(day), enabled);
}

bool Alarm::isEnabled()
{
    return enabled;
}

void Alarm::setEnabled(bool enabled)
{
    this->enabled = enabled;
}

void Alarm::setWeekday(Qt::DayOfWeek day, bool enabled)
{
    if (enabled) {
        weekdays << day;
    } else {
        weekdays.remove(day);
    }
}
