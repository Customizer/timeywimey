/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "settingsmanager.h"

SettingsManager::SettingsManager()
{
}

void SettingsManager::setValue(QString key, QString value)
{
    QSettings settings;
    settings.setValue(key, value);
}

QVariant SettingsManager::getValue(QString key)
{
    QSettings settings;
    return settings.value(key);
}

void SettingsManager::addAlarm(Alarm alarm)
{
    QSettings settings;
    settings.beginGroup("alarms");
    QStringList groups = settings.childGroups();
    int alarmNr = groups.length();
    settings.endGroup();

    saveAlarm(alarmNr, alarm);
}

void SettingsManager::saveAlarm(int alarmNr, Alarm alarm)
{
    QSettings settings;
    settings.beginGroup("alarms");

    settings.beginGroup(QStringLiteral("Alarm_%1").arg(alarmNr));
    settings.setValue("hour", alarm.getTime().toString("hh"));
    settings.setValue("minute", alarm.getTime().toString("mm"));
    settings.setValue("enabled", alarm.isEnabled() ? "true" : "false");

    settings.remove("weekdays");

    settings.beginGroup("weekdays");
    QSet<Qt::DayOfWeek> weekdays = alarm.getWeekdays();

    int counter = 0;
    for (Qt::DayOfWeek day : weekdays) {
        settings.setValue(QStringLiteral("day_%1").arg(counter), day);
        counter++;
    }
    settings.endGroup();
    settings.endGroup();
    settings.endGroup();

    emit alarmChanged(alarmNr);
}

void SettingsManager::deleteAlarm(int alarmNr)
{
    QList<Alarm> alarms = getAlarms();
    alarms.removeAt(alarmNr);

    QSettings settings;
    settings.remove("alarms");

    for (Alarm alarm : alarms) {
        addAlarm(alarm);
    }

    emit alarmRemoved(alarmNr);
}

void SettingsManager::setAlarmEnabled(int alarmNr, bool enabled)
{
    Alarm alarm = getAlarm(alarmNr);
    alarm.setEnabled(enabled);
    saveAlarm(alarmNr, alarm);
    emit alarmChanged(alarmNr);
}

Alarm SettingsManager::getAlarm(int alarmNr)
{
    QSettings settings;
    settings.beginGroup("alarms");
    settings.beginGroup(QStringLiteral("Alarm_%1").arg(alarmNr));
    QString hour = settings.value("hour").toString();
    QString minute = settings.value("minute").toString();
    bool enabled = settings.value("enabled").toString() == "true";

    settings.beginGroup("weekdays");
    QStringList dayKeys = settings.childKeys();
    QStringList weekdays;
    for (const QString key : dayKeys) {
        weekdays.push_back(settings.value(key).toString());
    }
    settings.endGroup();

    settings.endGroup();
    settings.endGroup();

    return Alarm(hour, minute, weekdays, enabled);
}

QList<Alarm> SettingsManager::getAlarms()
{
    QList<Alarm> alarms;
    for (int alarmNr = 0; alarmNr < getAlarmsCount(); alarmNr++) {
        alarms += getAlarm(alarmNr);
    }
    return alarms;
}

QList<Alarm> SettingsManager::getAlarmsExceptOne(int exceptionNr)
{
    QList<Alarm> alarms;
    for (int alarmNr = 0; alarmNr < getAlarmsCount(); alarmNr++) {
        if (alarmNr != exceptionNr) {
            alarms += getAlarm(alarmNr);
        }
    }
    return alarms;
}

int SettingsManager::getAlarmsCount()
{
    QSettings settings;
    settings.beginGroup("alarms");

    QStringList groups = settings.childGroups();
    int alarmCount = groups.length();

    settings.endGroup();

    return alarmCount;
}
