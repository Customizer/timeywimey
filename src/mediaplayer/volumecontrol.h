/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef VOLUMECONTROL_H
#define VOLUMECONTROL_H

#include <QObject>
#include <QTimer>

class VolumeControl : public QObject
{
    Q_OBJECT
public:
    explicit VolumeControl(QObject *parent = 0);
    enum FADING_DIRECTION {
        UNDEFINED,
        FADING_IN,
        FADING_OUT
    };

    void setFadingDuration(int msecs);

public slots:
    void startFadingIn();
    void startFadingOut();

signals:
    void volumeChanged(int volume);
    void fadeInFinished();
    void fadeOutFinished();

private slots:
    void fadeIn();
    void fadeOut();

private:
    int maxVolume = 90;
    int currentVolume = 0;
    int duration;
    FADING_DIRECTION state = UNDEFINED;
};

#endif // VOLUMECONTROL_H
