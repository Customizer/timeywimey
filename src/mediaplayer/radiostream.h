/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef RADIOSTREAM_H
#define RADIOSTREAM_H

#include <QObject>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <memory>

class RadioStream : public QIODevice
{
    Q_OBJECT
public:
    explicit RadioStream(bool noInternet, QObject *parent = 0);
    void startStreaming(QUrl url);
    void stopStreaming();

signals:
    void finishedBuffering();
    void newMetadata(QString content);
    void networkError(QNetworkReply::NetworkError error);
    void newRadioStation(QString radioStation);

public slots:
    void readyReadSlot();
    void streamFinished();

private:
    QNetworkAccessManager manager;
    QNetworkReply *reply = Q_NULLPTR;
    QDateTime bufferTime;
    bool finishedBufferingCalled = false;

    qint32 icyMetaInt = 0;
    qint64 byteCount = 0;

    // QIODevice interface
public:
    bool isSequential() const;
    qint64 pos() const;
    qint64 bytesAvailable() const;

protected:
    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);

};

#endif // RADIOSTREAM_H
