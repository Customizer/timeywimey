/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "radiostream.h"

#include <QDataStream>

RadioStream::RadioStream(bool noInternet, QObject *parent) : QIODevice(parent), manager(parent)
{
    bufferTime = QDateTime::currentDateTime();
    if (noInternet) {
        manager.setNetworkAccessible(QNetworkAccessManager::NotAccessible);
    }
}

void RadioStream::startStreaming(QUrl url)
{
    QNetworkRequest request(url);
    request.setRawHeader("Icy-Metadata", "1");
    bufferTime = QDateTime::currentDateTime();
    finishedBufferingCalled = false;
    reply = manager.get(request);
    connect(reply, &QNetworkReply::readyRead, this, &RadioStream::readyRead);
    connect(reply, &QNetworkReply::readyRead, this, &RadioStream::readyReadSlot);
    connect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>
            (&QNetworkReply::error), this, &RadioStream::networkError);
    connect(reply, &QNetworkReply::finished, this, &RadioStream::streamFinished);
    open(QIODevice::ReadOnly);
}

void RadioStream::stopStreaming()
{
    close();
    if (reply != Q_NULLPTR) {
        disconnect(reply, &QNetworkReply::readyRead, this, &RadioStream::readyRead);
        disconnect(reply, &QNetworkReply::readyRead, this, &RadioStream::readyReadSlot);
        disconnect(reply, static_cast<void(QNetworkReply::*)(QNetworkReply::NetworkError)>
                   (&QNetworkReply::error), this, &RadioStream::networkError);
        reply->close();
    }
    icyMetaInt = 0;
    byteCount = 0;
}

void RadioStream::readyReadSlot()
{
    // TODO add gui info about buffer status
    if (QDateTime::currentDateTime() > bufferTime.addMSecs(1500) &&
            !finishedBufferingCalled) {
        emit finishedBuffering();
        finishedBufferingCalled = true;
    }
}

void RadioStream::streamFinished()
{
    disconnect(reply, &QNetworkReply::finished, this, &RadioStream::streamFinished);
    reply->deleteLater();
    reply = Q_NULLPTR;
}

bool RadioStream::isSequential() const
{
    return reply->isSequential();
}

qint64 RadioStream::pos() const
{
    return reply->pos();
}

qint64 RadioStream::bytesAvailable() const
{
    return reply->bytesAvailable();
}

qint64 RadioStream::readData(char *data, qint64 maxlen)
{
    QDataStream radioStream(reply);
    QByteArray buffer;
    QDataStream bufferStream(&buffer, QIODevice::ReadOnly);
    qint64 contentLength = 0;

    QByteArray header = QString("icy-metaint").toUtf8();
    if (reply->hasRawHeader(header)) {
        QString metaInt = QString::fromUtf8(reply->rawHeader(header));
        icyMetaInt = metaInt.toInt();
    }

    while (contentLength <= maxlen && reply->size() > 0) {
        qint64 chunkLength = icyMetaInt - byteCount;
        if (contentLength + chunkLength > maxlen) {
            chunkLength = maxlen - contentLength;
        }
        if (chunkLength > reply->size()) {
            chunkLength = reply->size();
        }
        contentLength += chunkLength;
        byteCount += chunkLength;
        buffer.append(reply->read(chunkLength));

        if (contentLength >= maxlen || reply->size() <= 0) {
            break;
        }

        if (reply->size() >= icyMetaInt) {
            qint8 metadataLength;
            radioStream >> metadataLength;
            QString content = reply->read(metadataLength * 16);
            if (!content.isEmpty()) {
                emit newMetadata(content);

                /*
                 * We don't want to do this too often, so we update it only when updating the
                 * rest of the metadata.
                 */
                QByteArray header = QString("icy-name").toUtf8();
                if (reply->hasRawHeader(header)) {
                    emit newRadioStation(QString::fromUtf8(reply->rawHeader(header)));
                }
            }
            byteCount = 0;
        } else {
            break;
        }
    }

    bufferStream.readRawData(data, contentLength);
    return contentLength;
}

qint64 RadioStream::writeData(const char *data, qint64 len)
{
    return -1;
}
