/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#include "mediaplayer.h"

MediaPlayer::MediaPlayer(bool noInternet, bool testFile, QObject *parent) : QObject(parent),
    player(parent, QMediaPlayer::StreamPlayback),
    stateMachine(parent)
{
    QObject::connect(&player, &QMediaPlayer::mediaStatusChanged,
                     this, &MediaPlayer::mediaStatusChanged);
    this->noInternet = noInternet;
    this->testFile = testFile;
    stream = Q_NULLPTR;
    // DO i still need this?
    connect(&volume, &VolumeControl::volumeChanged, this, &MediaPlayer::volumeChanged);

    slumberTimer.setInterval(60 * 1000);
    connect(&slumberTimer, &QTimer::timeout, this, &MediaPlayer::checkSlumberState);

    initStateMachine();
}

void MediaPlayer::start()
{
    startIntern(800);
}

void MediaPlayer::stop()
{
    stop(800);
}

void MediaPlayer::startSlumberMode()
{
    if (!slumberTimer.isActive()) {
        start();
        slumberMinutes = 10;
        emit slumberTimeUpdate(slumberMinutes);
        slumberTimer.start();
    }
}

void MediaPlayer::setSlumberTime(QString minutes)
{
    slumberMinutes = minutes.toInt();
}

bool MediaPlayer::isPlaying()
{
    // TODO this seems to be wrong
    return player.state() == QMediaPlayer::PlayingState;
}

void MediaPlayer::setVolume(int volume)
{
    player.setVolume(volume);
}

void MediaPlayer::startAlarm()
{
    startIntern(30000);
}

void MediaPlayer::mediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    if (status == QMediaPlayer::MediaStatus::LoadedMedia) {
        player.play();
        emit startedPlaying();
    }
}

void MediaPlayer::networkError(QNetworkReply::NetworkError error)
{
    volumeChanged(0);
    player.stop();
    QMediaPlaylist *playlist = new QMediaPlaylist();
    playlist->setPlaybackMode(QMediaPlaylist::Loop);
    if (testFile) {
        playlist->addMedia(QUrl("qrc:/music/Jahzzar/Jahzzar_-_05_-_Siesta_shortVersion.mp3"));
    } else {
        playlist->addMedia(QUrl("qrc:/music/Jahzzar/Jahzzar_-_05_-_Siesta.mp3"));
    }
    player.setPlaylist(playlist);
    stopStreaming();
    volume.setFadingDuration(800);
    volume.startFadingIn();

    emit newArtist("Jahzzar");
    emit newTitle("Siesta (local fallback audio file)");
    emit newAlbumArt("");
    emit newRadioStation("");
}

void MediaPlayer::volumeChanged(int volume)
{
    player.setVolume(volume);
}

void MediaPlayer::newMetadata(QString content)
{
    QRegExp artistAndTitleRegEx("StreamTitle='(.*)';StreamUrl.*");
    QRegExp albumArtUrlRegEx("StreamTitle='.*';StreamUrl='(.*)'");

    int pos = artistAndTitleRegEx.indexIn(content);
    if (pos != -1) {
        QString artistAndTitle = artistAndTitleRegEx.cap(1);
        QStringList split = artistAndTitle.split(" - ");
        emit newArtist(split.at(0));
        emit newTitle(split.at(1));
    }

    pos = albumArtUrlRegEx.indexIn(content);
    if (pos != -1) {
        QString albumArt = albumArtUrlRegEx.cap(1);
        emit newAlbumArt(albumArt);
    }
}

void MediaPlayer::checkSlumberState()
{
    slumberMinutes--;
    emit slumberTimeUpdate(slumberMinutes);
    if (slumberMinutes == 0) {
        stop(30000);
    }
}

void MediaPlayer::bufferingStateEntered()
{
    volumeChanged(0);
    startStreaming();
}

void MediaPlayer::fadingStateEntered()
{
    volume.setFadingDuration(nextFadingDuration);
    if (nextFadingDirection == VolumeControl::FADING_DIRECTION::FADING_IN) {
        volume.startFadingIn();
    } else {
        volume.startFadingOut();
    }
}

void MediaPlayer::startLoadingStream()
{
    player.setMedia(QMediaContent(), stream);
}

void MediaPlayer::initStateMachine()
{
    QState *stopped = new QState();
    QState *buffering = new QState();
    QState *fading = new QState();
    QState *playing = new QState();

    connect(buffering, &QState::entered, this, &MediaPlayer::bufferingStateEntered);

    connect(fading, &QState::entered, this, &MediaPlayer::fadingStateEntered);

    stopped->addTransition(this, &MediaPlayer::starting, buffering);

    buffering->addTransition(this, &MediaPlayer::startedPlaying, fading);
    QSignalTransition *bufferingFinishedTrans = new QSignalTransition(this,
                                                                      &MediaPlayer::finishedBuffering);
    buffering->addTransition(bufferingFinishedTrans);
    connect(bufferingFinishedTrans, &QSignalTransition::triggered, this,
            &MediaPlayer::startLoadingStream);

    fading->addTransition(&volume, &VolumeControl::fadeInFinished, playing);
    fading->addTransition(this, &MediaPlayer::stopped, stopped);

    QSignalTransition *fadingOutTrans = new QSignalTransition(this,
                                                              &MediaPlayer::stopping);
    connect(fadingOutTrans, &QSignalTransition::triggered, this,
            &MediaPlayer::fadingStateEntered);
    fading->addTransition(fadingOutTrans);

    QSignalTransition *fadingInTrans = new QSignalTransition(this,
                                                             &MediaPlayer::starting);
    connect(fadingInTrans, &QSignalTransition::triggered, this,
            &MediaPlayer::fadingStateEntered);
    fading->addTransition(fadingInTrans);

    playing->addTransition(this, &MediaPlayer::stopping, fading);

    QSignalTransition *fadeOutFinishedTrans = new QSignalTransition(&volume,
                                                                    &VolumeControl::fadeOutFinished);
    fading->addTransition(fadeOutFinishedTrans);
    connect(fadeOutFinishedTrans, &QSignalTransition::triggered, this, &MediaPlayer::stopIntern);

    stateMachine.addState(stopped);
    stateMachine.addState(buffering);
    stateMachine.addState(playing);
    stateMachine.addState(fading);

    stateMachine.setInitialState(stopped);
    stateMachine.start();
}

void MediaPlayer::startIntern(int fadingDuration)
{
    nextFadingDuration = fadingDuration;
    nextFadingDirection = VolumeControl::FADING_DIRECTION::FADING_IN;
    emit starting();
}

void MediaPlayer::stop(int fadingDuration)
{
    nextFadingDuration = fadingDuration;
    nextFadingDirection = VolumeControl::FADING_DIRECTION::FADING_OUT;
    slumberTimer.stop();
    emit stopping();
}

void MediaPlayer::stopIntern()
{
    emit newArtist("");
    emit newTitle("");
    emit newAlbumArt("");

    player.stop();
    player.setMedia(QMediaContent());
    stream->stopStreaming();
    stream->deleteLater();
    stream = Q_NULLPTR;

    // TODO do i still need this signal?
    emit stopped();
}

void MediaPlayer::startStreaming()
{
    if (stream != Q_NULLPTR) {
        stream->deleteLater();
    }
    stream = new RadioStream(noInternet, this);
    connect(stream, &RadioStream::finishedBuffering, this, &MediaPlayer::finishedBuffering);
    connect(stream, &RadioStream::newMetadata, this, &MediaPlayer::newMetadata);
    connect(stream, &RadioStream::newRadioStation, this, &MediaPlayer::newRadioStation);
    connect(stream, &RadioStream::networkError, this, &MediaPlayer::networkError);
    stream->startStreaming(QUrl("http://stream-uk1.radioparadise.com/mp3-192"));
}


void MediaPlayer::stopStreaming()
{
    disconnect(stream, &RadioStream::finishedBuffering, this, &MediaPlayer::finishedBuffering);
    disconnect(stream, &RadioStream::newMetadata, this, &MediaPlayer::newMetadata);
    disconnect(stream, &RadioStream::newRadioStation, this, &MediaPlayer::newRadioStation);
    disconnect(stream, &RadioStream::networkError, this, &MediaPlayer::networkError);
    stream->stopStreaming();
}
