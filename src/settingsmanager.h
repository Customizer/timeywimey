/****************************************************************************
**
**  TimeyWimey - a Raspberry Pi alarm clock
**  Copyright (C) 2016-2017  Arne Augenstein
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with this program.  If not, see <http://www.gnu.org/licenses/>.
**
****************************************************************************/
#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H

#include <QObject>
#include <QSettings>
#include <QDateTime>

#include "alarms/alarm.h"

class SettingsManager : public QObject
{
    Q_OBJECT
public:
    SettingsManager();

    Q_INVOKABLE void setValue(QString key, QString value);

    Q_INVOKABLE QVariant getValue(QString key);

    void addAlarm(Alarm alarm);

    void saveAlarm(int alarmNr, Alarm alarm);

    Q_INVOKABLE void deleteAlarm(int alarmNr);

    Q_INVOKABLE void setAlarmEnabled(int alarmNr, bool enabled);

    Alarm getAlarm(int alarmNr);

    QList<Alarm> getAlarms();

    QList<Alarm> getAlarmsExceptOne(int exceptionNr);

signals:
    void alarmChanged(int alarmNr);
    void alarmRemoved(int alarmNr);

private:
    int getAlarmsCount();
};

#endif // SETTINGSMANAGER_H
